<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['buyer_id'];

    public function buyer(){
        return $this->belongsTo(Buyer::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function orderSum()
    {
        $price = $this->orderItems->map(function($orderItems) {
            return $orderItems->productSum();
        })->all();
        return array_sum($price);
    }

}
