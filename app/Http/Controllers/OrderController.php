<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Order;
use App\Buyer;
use App\OrderItem;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =  $request->input();
        $buyerId = $data['buyerId'] ?? false;
        if($buyer = Buyer::find($buyerId)) {
            $order = Order::create(['buyer_id' => $data['buyerId']]);
            foreach ($data['orderItems'] as $item) {
                $orderItem = new OrderItem();
                $orderItem->product_id = $item['productId'];
                $orderItem->quantity = $item['productQty'];
                $orderItem->discount = $item['productDiscount'];
                $orderItem->order_id = $order->id;
                $orderItem->save();
            }
        }

        return new Response([
            'result' => $buyer ? 'success' : 'fail',
            'message' => $buyer ? 'OK' : 'Buyer not found'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OrderResource(Order::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $orderId = $data['orderId'] ?? false;
        if($order = Order::find($orderId)) {
            $order->orderItems()->delete();
            foreach ($data['orderItems'] as $item) {
                $order->OrderItems()->create([
                    'product_id' => $item['productId'],
                    'quantity' => $item['productQty'],
                    'discount' => $item['productDiscount'],
                    'order_id' => $order->id,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
