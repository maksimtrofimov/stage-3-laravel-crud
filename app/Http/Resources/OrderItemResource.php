<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\OrderItem;
use Illuminate\Support\Collection;

class OrderItemResource extends JsonResource
{
    public static function present(OrderItem $orderItems): array
    {
        return [
            'productName' => $orderItems->product->name,
            'productQty' => $orderItems->quantity,
            'productPrice' => $orderItems->product->price,
            'productDiscount' => $orderItems->discount,
            'productSum' => $orderItems->productSum(),
        ];

    }

    public static function presentCollection(Collection $collection): array
    {
        return $collection->map(function($orderItems) {
            return self::present($orderItems);
        })->all();;
    }
}
