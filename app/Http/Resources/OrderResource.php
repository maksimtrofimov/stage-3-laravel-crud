<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $this->orderSum(),
            'orderItems' => OrderItemResource::presentCollection($this->orderItems),
            'buyer' => [
                'buyerFullName' => $this->buyer->getFullName(),
                'buyerAddress' => $this->buyer->getFullAddress(),
                'buyerPhone' => $this->buyer->phone,
            ],
        ];
    }


}
