<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(5, 10),
        'discount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0.01, $max = 0.75)
    ];
});
