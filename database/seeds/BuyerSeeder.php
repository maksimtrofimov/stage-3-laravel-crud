<?php

use Illuminate\Database\Seeder;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = DB::table('my_products')->get()->toArray();
        factory(\App\Buyer::class, 5)->create()->each(function ($buyer) use ($products) {
            $order = factory(\App\Order::class, 3)
                ->make(['buyer_id' => null]);
            $buyer->orders()->saveMany($order);

            $order->each(function ($order) use ($products) {
                $orderItems = factory(\App\OrderItem::class, 3)
                    ->make([
                        'order_id' => null,
                        'product_id' => array_rand($products, 1)
                    ]);
                $order->orderItems()->saveMany($orderItems);
            });
        });
    }
}


